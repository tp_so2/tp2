//
// Created by lucas on 29/4/21.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <netcdf.h>

#include <locale.h>
#include <time.h>
#include <math.h>

#include <png.h>

/* This is the name of the data file we will read. */
#define FILE_NAME "../data/OR_ABI-L2-MCMIPF-M6_G16_s20211181500183_e20211181509496_c20211181509595.nc"
#define OUT_FILE_NAME "../img/result.png"

/* Handle errors by printing an error message and exiting with a non-zero status. */
#define ERRCODE             2
#define ERR(e) { printf("Error: %s\n", nc_strerror(e)); exit(ERRCODE); }

/* Date format */
#define INPUT_DATE_FORMAT   "%Y-%m-%dT%H:%M:%S.%fZ"
#define OUTPUT_DATE_FORMAT  "%d/%m/%Y %T"

/* Channel names */
#define RED_CHANNEL         "CMI_C07"
#define GREEN_CHANNEL       "CMI_C06"
#define BLUE_CHANNEL        "CMI_C05"

/* Channel info */
#define CH5_INFO            "band_wavelength_C05"
#define CH6_INFO            "band_wavelength_C06"
#define CH7_INFO            "band_wavelength_C07"
#define LONG_NAME           "long_name"
#define UNIT                "units"

/* Attributes name */
#define SCAN_START          "time_coverage_start"
#define SCAN_END            "time_coverage_end"
#define FILE_CREATED        "date_created"
#define PROJECT             "project"
#define PLATAFORM           "platform_ID"

/* We are reading 2D data, a 5424 x 5424 grid. */
#define NY                  5424
#define NX                  5424
#define RADIUS              (NX/2)

/* Scale and offset factors */
#define CH_SCALE            "scale_factor"
#define CH_OFFSET           "add_offset"

/* Limit data range */
#define MAX_LIMIT           1
#define MIN_LIMIT           0

/* Gamma factor */
#define GAMMA               0.4

/* This will be the netCDF ID for the file and data variable. */
int ncid, R, G, B;

/* Matrix of read data */
short ch5_data_read[NX][NY], ch6_data_read[NX][NY], ch7_data_read[NX][NY];
double ch5_data[NX][NY], ch6_data[NX][NY], ch7_data[NX][NY];

/* Loop indexes, and error handling. */
int retval;


/* PNG variables */
png_structp png_ptr;
png_infop info_ptr;
png_bytep *row;

/* Functions declarations */
void printAttrInfo();

void printChannelsInfo();

void getChannelsData();

void scaleNormalizeClipChannelsData();

double clipData(double data, double max, double min);

double gammaCorrection(double data);

void writePngFile(char *file_name);

double radius(double x, double y);

/* Main function */
int main() {

    /* Open the file. NC_NOWRITE tells netCDF we want read-only access to the file */
    if ((retval = nc_open(FILE_NAME, NC_NOWRITE, &ncid)) != NC_NOERR) {
        ERR(retval);
    }

    /* Print attribute info */
    printAttrInfo();

    /* Print channel info */
    printChannelsInfo();

    /* Get varid for each channel */
    getChannelsData();

    /* Scale and normalize data */
    scaleNormalizeClipChannelsData();

    /* Creates .png img with results */
    writePngFile(OUT_FILE_NAME);

    /* Close the file, freeing all resources */
    if ((retval = nc_close(ncid)) != NC_NOERR) {
        ERR(retval);
    }
}

/**
 * Print attribute info of open file
 */
void printAttrInfo() {
    size_t scan_start_len = 0;
    size_t scan_end_len = 0;
    size_t file_created_len = 0;
    size_t project_len = 0;
    size_t platform_len = 0;

    /* Get attribute length */
    if ((retval = nc_inq_attlen(ncid, NC_GLOBAL, SCAN_START, &scan_start_len)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_attlen(ncid, NC_GLOBAL, SCAN_END, &scan_end_len)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_attlen(ncid, NC_GLOBAL, FILE_CREATED, &file_created_len)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_attlen(ncid, NC_GLOBAL, PROJECT, &project_len)) != NC_NOERR) {
        ERR(retval)
    }
    if ((retval = nc_inq_attlen(ncid, NC_GLOBAL, PLATAFORM, &platform_len)) != NC_NOERR) {
        ERR(retval);
    }

    /* Allocates necessary memory for each attribute */
    char *scan_start = calloc(scan_start_len, sizeof(char));
    char *scan_end = calloc(scan_end_len, sizeof(char));
    char *file_created = calloc(file_created_len, sizeof(char));
    char *project = calloc(project_len, sizeof(char));
    char *platform = calloc(platform_len, sizeof(char));

    /* Get attribute value */
    if ((retval = nc_get_att_text(ncid, NC_GLOBAL, SCAN_START, scan_start)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_text(ncid, NC_GLOBAL, SCAN_END, scan_end)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_text(ncid, NC_GLOBAL, FILE_CREATED, file_created)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_text(ncid, NC_GLOBAL, PROJECT, project)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_text(ncid, NC_GLOBAL, PLATAFORM, platform)) != NC_NOERR) {
        ERR(retval);
    }

    /* Format attributes */
    struct tm scan_start_date, scan_end_date, file_created_date;
    strptime(scan_start, INPUT_DATE_FORMAT, &scan_start_date);
    strptime(scan_end, INPUT_DATE_FORMAT, &scan_end_date);
    strptime(file_created, INPUT_DATE_FORMAT, &file_created_date);

    strftime(scan_start, strlen(scan_start), OUTPUT_DATE_FORMAT, &scan_start_date);
    strftime(scan_end, strlen(scan_end), OUTPUT_DATE_FORMAT, &scan_end_date);
    strftime(file_created, strlen(file_created), OUTPUT_DATE_FORMAT, &file_created_date);

    /* Prints formated attributes */
    printf("|---------------------------------------|\n");
    printf("|          %s - %s                   |\n", project, platform);
    printf("|---------------------------------------|\n");
    printf("| Scan Start    : %s   |\n", scan_start);
    printf("| Scan End      : %s   |\n", scan_end);
    printf("| File created  : %s   |\n", file_created);
    printf("| Scan Duration : %.2f minutes          |\n",
           difftime(timegm(&scan_end_date), timegm(&scan_start_date)) / 60);
    printf("|---------------------------------------|\n\n");

    /* Free memory */
    free(scan_start);
    free(scan_end);
    free(file_created);
    free(project);
    free(platform);
}

/**
 * Print channels info of open file
 */
void printChannelsInfo() {
    int ch5, ch6, ch7;

    /* Get the varid of the data variable, based on its name */
    if ((retval = nc_inq_varid(ncid, CH5_INFO, &ch5)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_varid(ncid, CH6_INFO, &ch6)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_varid(ncid, CH7_INFO, &ch7)) != NC_NOERR) {
        ERR(retval);
    }

    size_t ch5_info_len, ch6_info_len, ch7_info_len;
    size_t ch5_unit_len, ch6_unit_len, ch7_unit_len;

    /* Get attribute length */
    if ((retval = nc_inq_attlen(ncid, ch5, LONG_NAME, &ch5_info_len)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_attlen(ncid, ch6, LONG_NAME, &ch6_info_len)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_attlen(ncid, ch7, LONG_NAME, &ch7_info_len)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_attlen(ncid, ch5, UNIT, &ch5_unit_len)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_attlen(ncid, ch6, UNIT, &ch6_unit_len)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_attlen(ncid, ch7, UNIT, &ch7_unit_len)) != NC_NOERR) {
        ERR(retval);
    }

    /* Allocates necessary memory for each attribute */
    char *ch5_info = calloc(ch5_info_len, sizeof(char));
    char *ch6_info = calloc(ch6_info_len, sizeof(char));
    char *ch7_info = calloc(ch7_info_len, sizeof(char));
    char *ch5_unit = calloc(ch5_unit_len, sizeof(char));
    char *ch6_unit = calloc(ch6_unit_len, sizeof(char));
    char *ch7_unit = calloc(ch7_unit_len, sizeof(char));

    /* Get var attribute value */
    if ((retval = nc_get_att_text(ncid, ch5, LONG_NAME, ch5_info)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_text(ncid, ch6, LONG_NAME, ch6_info)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_text(ncid, ch7, LONG_NAME, ch7_info)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_text(ncid, ch5, UNIT, ch5_unit)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_text(ncid, ch6, UNIT, ch6_unit)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_text(ncid, ch7, UNIT, ch7_unit)) != NC_NOERR) {
        ERR(retval);
    }

    float ch5WL, ch6WL, ch7WL;

    /* Get var attribute value */
    if ((retval = nc_get_var_float(ncid, ch5, &ch5WL)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_var_float(ncid, ch6, &ch6WL)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_var_float(ncid, ch7, &ch7WL)) != NC_NOERR) {
        ERR(retval);
    }

    /* Prints channel info */
    printf("|----------------------------------------------|\n");
    printf("|                 Channel info                 |\n");
    printf("|----------------------------------------------|\n");
    printf("| %s is %.2f %s     |\n", ch5_info, ch5WL, ch5_unit);
    printf("| %s is %.2f %s     |\n", ch6_info, ch6WL, ch6_unit);
    printf("| %s is %.2f %s     |\n", ch7_info, ch7WL, ch7_unit);
    printf("|----------------------------------------------|\n\n");

    /* Free memory */
    free(ch5_info);
    free(ch6_info);
    free(ch7_info);
    free(ch5_unit);
    free(ch6_unit);
    free(ch7_unit);
}

/**
 * Get channel data
 */
void getChannelsData() {
    /* Get the varid of the data variable, based on its name */
    if ((retval = nc_inq_varid(ncid, RED_CHANNEL, &R)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_varid(ncid, GREEN_CHANNEL, &G)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_inq_varid(ncid, BLUE_CHANNEL, &B)) != NC_NOERR) {
        ERR(retval);
    }

    /* Read the data */
    if ((retval = nc_get_var_short(ncid, R, &ch7_data_read[0][0])) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_var_short(ncid, G, &ch6_data_read[0][0])) != NC_NOERR) {
        ERR(retval)
    }
    if ((retval = nc_get_var_short(ncid, B, &ch5_data_read[0][0])) != NC_NOERR) {
        ERR(retval);
    }
}

/**
 * Scale each channel by the own scale factor and offset.
 * Normalize each channel by the appropriate range of values  e.g. R = (R-minimum)/(maximum-minimum)
 * [ CH7 ] R = (R-273)/(333-273)
 * [ CH6 ] G = (G-0)/(1-0) Isn't necessary normalize
 * [ CH5 ] B = (B-0)/(0.75-0)
 * And apply range limits for each channel. RGB values must be between 0 and 1 with clip function
 */
void scaleNormalizeClipChannelsData() {
    double ch5_scale, ch6_scale, ch7_scale;
    double ch5_offset, ch6_offset, ch7_offset;

    /* Read the data */
    if ((retval = nc_get_att_double(ncid, B, CH_SCALE, &ch5_scale)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_double(ncid, G, CH_SCALE, &ch6_scale)) != NC_NOERR) {
        ERR(retval)
    }
    if ((retval = nc_get_att_double(ncid, R, CH_SCALE, &ch7_scale)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_double(ncid, B, CH_OFFSET, &ch5_offset)) != NC_NOERR) {
        ERR(retval);
    }
    if ((retval = nc_get_att_double(ncid, G, CH_OFFSET, &ch6_offset)) != NC_NOERR) {
        ERR(retval)
    }
    if ((retval = nc_get_att_double(ncid, R, CH_OFFSET, &ch7_offset)) != NC_NOERR) {
        ERR(retval);
    }

    /* Scale, normalize and clip data */
    for (int i = 0; i < NX; ++i) {
        for (int j = 0; j < NY; ++j) {
            // Check boundary
            if (radius(i, j) <= RADIUS) {
                ch5_data[i][j] = clipData((((double) ch5_data_read[i][j] * ch5_scale) + ch5_offset) / 0.75, MAX_LIMIT,
                                          MIN_LIMIT);
                ch6_data[i][j] = clipData(((double) ch6_data_read[i][j] * ch6_scale) + ch6_offset, MAX_LIMIT,
                                          MIN_LIMIT);
                ch7_data[i][j] = gammaCorrection(
                        clipData(((((double) ch7_data_read[i][j] * ch7_scale) + ch7_offset - 273) / (333 - 273)),
                                 MAX_LIMIT, MIN_LIMIT));
            }
        }
    }
}

/**
 * Given an interval, values outside the interval are clipped to the interval edges
 * @param data to clip between ranges
 * @param max value to clip
 * @param min value to clip
 * @return value clipped
 */
double clipData(double data, double max, double min) {
    if (data < min) {
        data = min;
    }
    if (data > max) {
        data = max;
    }
    return data;
}

/**
 * Apply the gamma correction to any value
 * @param data to apply gamma fix
 * @return value fixed
 */
double gammaCorrection(double data) {
    return pow(data, 1 / GAMMA);
}

/**
 * Creates .png file with result of computation
 * http://www.libpng.org/pub/png/libpng-1.2.5-manual.html#section-4
 * http://zarb.org/~gc/html/libpng.html
 * http://www.labbookpages.co.uk/software/imgProc/libPNG.html
 * https://stackoverflow.com/a/65409282/14311150
 * @param file_name of file to write
 */
void writePngFile(char *file_name) {
    /* Create file */
    FILE *fp;
    if ((fp = fopen(file_name, "wb")) == NULL) {
        fprintf(stderr, "File %s could not be opened for writing\n", file_name);
        goto finalise;
    }

    /* Initialize png pointer */
    if ((png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL)) == NULL) {
        fprintf(stderr, "The creation of png_ptr failed\n");
        goto finalise;
    }

    /* Initialize info png pointer */
    if ((info_ptr = png_create_info_struct(png_ptr)) == NULL) {
        fprintf(stderr, "The creation of info_png_ptr failed\n");
        goto finalise;
    }

    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during init_io\n");
        goto finalise;
    }

    png_init_io(png_ptr, fp);

    /* Write header */
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during writing header\n");
        goto finalise;
    }

    png_set_IHDR(png_ptr, info_ptr, NX, NY,
                 8, PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info(png_ptr, info_ptr);


    /* Write bytes */
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during writing bytes\n");
        goto finalise;
    }

    // Allocate memory for one row (3 bytes per pixel - RGB)
    row = (png_bytep *) calloc(1, NY * sizeof(png_bytep));
    for (int i = 0; i < NY; i++) {
        row[i] = (png_byte *) calloc(1, 3 * NX);
    }

    // Write image data
    for (int y = 0; y < NY; y++) {
        for (int x = 0; x < NX; x++) {
            // Check boundary
            if (radius(x, y) <= RADIUS) {
                row[y][x * 3] = (png_byte) (ch7_data[y][x] * 255); // R
                row[y][x * 3 + 2] = (png_byte) (ch5_data[y][x] * 255); // B
                row[y][x * 3 + 1] = (png_byte) (ch6_data[y][x] * 255); // G
            }
        }
    }

    png_write_image(png_ptr, row);

    /* End write */
    if (setjmp(png_jmpbuf(png_ptr))) {
        fprintf(stderr, "Error during end of write\n");
        goto finalise;
    }

    png_write_end(png_ptr, NULL);
    printf("Output img created in %s\n", OUT_FILE_NAME);

    /* Cleanup heap allocation */
    finalise:
    if (fp != NULL) fclose(fp);
    if (info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
    if (png_ptr != NULL) png_destroy_write_struct(&png_ptr, (png_infopp) NULL);
    if (row != NULL) free(row);
}

/**
 * Calculates distance to center of an point (x, y)
 * @param x x‐coordinate
 * @param y y‐coordinate
 * @return distance to center
 */
double radius(double x, double y) {
    return hypot(fabs((x - RADIUS)), fabs(y - RADIUS));
}