import numpy as np
from datetime import datetime

from netCDF4 import Dataset
from pyproj import Proj
import xarray
import matplotlib.pyplot as plt
import pandas as pd

FILE = '../data/OR_ABI-L2-MCMIPF-M6_G16_s20211181500183_e20211181509496_c20211181509595.nc'

C = xarray.open_dataset(FILE)

# Scan's start time, converted to datetime object
scan_start = datetime.strptime(C.time_coverage_start, '%Y-%m-%dT%H:%M:%S.%fZ')

# Scan's end time, converted to datetime object
scan_end = datetime.strptime(C.time_coverage_end, '%Y-%m-%dT%H:%M:%S.%fZ')

# File creation time, convert to datetime object
file_created = datetime.strptime(C.date_created, '%Y-%m-%dT%H:%M:%S.%fZ')

# The 't' variable is the scan's midpoint time
# I'm not a fan of numpy datetime, so I convert it to a regular datetime object
midpoint = str(C['t'].data)[:-8]
scan_mid = datetime.strptime(midpoint, '%Y-%m-%dT%H:%M:%S.%f')

print('Scan Start    : %s' % scan_start)
print('Scan midpoint : %s' % scan_mid)
print('Scan End      : %s' % scan_end)
print('File Created  : %s' % file_created)
print('Scan Duration : %.2f minutes' % ((scan_end-scan_start).seconds/60))

# Confirm that each band is the wavelength we are interested in
for band in [7, 6, 5]:
    print("%s is %.2f %s" % (C['band_wavelength_C%02d' % band].long_name,
                             C['band_wavelength_C%02d' % band][0],
                             C['band_wavelength_C%02d' % band].units))

# Load the three channels into appropriate R, G, and B variables
R = C['CMI_C07'].data
G = C['CMI_C06'].data
B = C['CMI_C05'].data

# RsinNAN = R[np.logical_not(np.isnan(R))]
# GsinNAN = G[np.logical_not(np.isnan(G))]
# BsinNAN = B[np.logical_not(np.isnan(B))]

# Normalize each channel by the appropriate range of values  e.g. R = (R-minimum)/(maximum-minimum)
R = (R-273)/(333-273)
G = (G-0)/(1-0)
B = (B-0)/(0.75-0)

# print(pd.DataFrame(R[2712]))

# Apply range limits for each channel. RGB values must be between 0 and 1
R = np.clip(R, 0, 1)
G = np.clip(G, 0, 1)
B = np.clip(B, 0, 1)

# print(R[2712][2712])
# print(G[2712][2712])
# print(B[2712][2712])

# Apply the gamma correction to Red channel.
#   corrected_value = value^(1/gamma)
gamma = 0.4
R = np.power(R, 1/gamma)

# The final RGB array :)
RGB = np.dstack([R, G, B])

# Print result
plt.imsave('../img/python_result.png', RGB)
