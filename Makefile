CC = gcc
CFLAGS = -Werror -Wall -pedantic -Wextra -Wunused -Wconversion -std=gnu11 -D _GNU_SOURCE
NCFLAGS = -lnetcdf
LCFLAGS = -lm -lpng
PCFLAGS = -fopenmp
OCFLAGS = -O3

default: all

all: secuencial secuencialO3 paralelo paraleloO3 paraleloV2 paraleloFast

secuencial:
	$(CC) $(CFLAGS) $(NCFLAGS) $(LCFLAGS) -o bin/secuencial src/secuencial.c

secuencialO3:
	$(CC) $(CFLAGS) $(NCFLAGS) $(LCFLAGS) $(OCFLAGS) -o bin/secuencialO3 src/secuencial.c

paralelo:
	$(CC) $(CFLAGS) $(NCFLAGS) $(LCFLAGS) $(PCFLAGS) -o bin/paralelo src/paralelo.c

paraleloO3:
	$(CC) $(CFLAGS) $(NCFLAGS) $(LCFLAGS) $(PCFLAGS) $(OCFLAGS) -o bin/paraleloO3 src/paralelo.c

paraleloV2:
	$(CC) $(CFLAGS) $(NCFLAGS) $(LCFLAGS) $(PCFLAGS) $(OCFLAGS) -o bin/paraleloV2 src/paraleloV2.c

paraleloFast:
	$(CC) $(CFLAGS) $(NCFLAGS) $(LCFLAGS) $(PCFLAGS) -Ofast -o bin/paraleloV2Fast src/paraleloV2.c

clean:
	rm -rf bin/secuencial bin/secuencialO3 bin/paralelo bin/paraleloO3 bin/paraleloV2 bin/paraleloV2Fast