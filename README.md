TP2 Sistemas Operativos 2 
### Ingeniería en Computación - FCEFyN - UNC
# Memoria compartida

## Introducción
Los niveles de integración electrónica han permitido la generación de procesadores de arquitecturas multiprocesos, multicore, híbridos y ahora many integrated core (MIC). Este avance hace necesario que los programadores cuenten con un profundo conocimiento del hardware sobre el que se ejecutan sus programas, y que dichos programas ya no pueden ser monoproceso.

Entre las técnicas y estándares más utilizados para sistemas de memoria compartida y memoria distribuida, se encuentra OpenMP y MPI respectivamente.

## Objetivo
El objetivo del presente trabajo práctico es que el estudiante sea capaz diseñar una solución que utilice el paradigma de memoria distribuida, utilizando OpenMP, en lenguaje C.

## Desarrollo
### Requerimientos
Para realizar el presente trabajo práctico, es necesario instalar la librerías _NetCDF4_ en el sistema sobre el cual se diseñe, desarrolle y pruebe la solución al problema. Estas librerías permiten compartir información tecnológica y científica en un formato auto definido, independiente  de la arquitectura del sistema y también definen un formato de datos que se transformó en estándar abierto. La librería tiene versiones en Fortran, Python, Java y C, siendo estas últimas las que vamos a utilizar en este trabajo.
Como ejemplo del uso de este formato de datos se tiene la red de de radares meteorológicos [NexRad](https://www.ncdc.noaa.gov/data-access/radar-data/nexrad) y la constelación de satélites [GOES](https://www.ncdc.noaa.gov/data-access/satellite-data/goes-r-series-satellites), ambos disponibles públicamente.    
Si bien esta librería se encuentra en la mayoría de los repositorios de las distribuciones GNU/Linux, se recomienda, dado que buscamos optimizar los tiempos de procesamiento, se recomienda que se [compilen e instalen manualmente](https://www.unidata.ucar.edu/software/netcdf/docs/building_netcdf_fortran.html). 
También se provee de un archivo base para el uso y lectura del archivo NetCDF.

### Problema a desarrollar
Como es sabido de público conocimiento, múltiples grandes incendios han ocurrido tanto en nuestra patagonia como en bosques del Amazonas y el hemisferio honte en la última década. Estos incendios no sólo provocan perdidas materiales y de biodiversidad, sino que también múltiples pérdidas humanas. Es por esto que el monitoreo en "Tiempo Real" de estas catástrofes se ha vuelto de suma importancia. El satélite geoestacionario GOES16, posee un instrumento que se llama ABI, que obtiene 16 canales de todo el globo cada diez minutos.
Estos datos se descargan a tierra y se ponen en disponibilidad a los usuarios. La forma más común de acceder a ellos, es a travñez del _buket S3_ de acceso libre y gratuito. cada archivo tiene un formato como el siguiente ejemplo:

```bash
ABI-L2-MCMIPF/2021/066/16/OR_ABI-L2-MCMIPF-M6_G16_s20210661636116_e20210661638489_c20210661638589.nc
```

El nombre del archivo nos informa del instrumento que se utilizó el instrumento **ABI**, que es un nivel de dato **L2**, que se trata del producto **CMIPF-M6**, el satélite que lo creó **G16**, el _timestamp_ de la creación del archivo, el inicio del barrido y el final del mismo.
Una característica de este producto, es que posee todas las bandas que genera el instrumento ABI, contenido en un único archivo NetCDF.

Realizando combinaciones y operaciones sobre estos canales, se pueden generar distintos productos aplicables en distintas aplicaciones. Para el caso de un producto para detectar incendios, se utilizan los siguientes:


|          -    | **RED**      | **GREEN**      | **BLUE**     |
|---------------|:------------:|:--------------:|:------------:|
| **Name**      | Shortwave Window | Cloud Partilce Size | Snow/Ice |
| **Wavelength**| 3.9 &#181;m | 2.2 &#181;m   | 1.6 &#181;m |
| **Channel**   |      7       |       6        |      5       |
| **Units**     | Temperature (K) | Reflectance | Reflectance |
| **Range of Values**| 273.15-333.15 | 0-1 | 0-1|
| **Gamma Correction**| 0.4 |none|none|

Y se obtiene una imagen similar a la siguiente

![GOES_FIRE](img/index.png)

### Algoritmo ejemplo
El siguiente ejemplo desarrolllado en python describiendo paso a paso lo que ustedes tienen que hacer, pero en C. 

1 - Descargar de AWS S3 un set de datos de GOES 16, preferiblemente en horario de entre 14 y 18 UTC, utilizando la [aws_cli](https://aws.amazon.com/es/cli/)
```bash
aws s3 cp s3://noaa-goes16/ABI-L2-MCMIPF/2021/066/16/OR_ABI-L2-MCMIPF-M6_G16_s20210661636116_e20210661638489_c20210661638589.nc . --no-sign-request
``` 
Luego se abre el archivo, y se _leen_ los canales de interés.
```python
# Open the Netcdf
C = Dataset(file.nc)

# Load the three channels into appropriate R, G, and B variables
R = C['CMI_C07'][:]
G = C['CMI_C06'][:]
B = C['CMI_C05'][:]
```

La _receta_ para obtener el producto deseado

```python
# Normalizar cada canal: C = (C-minimum)/(maximum-minimum)
R = (R-273)/(333-273)
G = (G-0)/(1-0)
B = (B-0)/(0.75-0)

# Apply range limits for each channel. RGB values must be between 0 and 1
R = np.clip(R, 0, 1)
G = np.clip(G, 0, 1)
B = np.clip(B, 0, 1)

# Apply the gamma correction to Red channel.
#   corrected_value = value^(1/gamma)
gamma = 0.4
R = np.power(R, 1/gamma)
```
Y listo!

Se pide además que se guarde el resultado en un formado de imagenes (png por ejemplo).

### Restricciones
Se debe considerar que cada canal es una matriz, pero sólo hay datos dentro de la circunferencia de la tierra. No se deben realizar operaciones fuera de este.
El diseño debe contemplar toda situación no descripta en el presente documento y se debe hacer un correcto manejo de errores. 

### Criterios de Corrección
- Se debe compilar el código con los flags de compilación: 
     -Wall -Pedantic -Werror -Wextra -Wconversion -std=gnu11
- La correcta gestion de memoria.
- Dividir el código en módulos de manera juiciosa.
- Estilo de código.
- Manejo de errores
- El código no debe contener errores, ni warnings.
- El código no debe contener errores de cppcheck.

## Entrega
Como metodología para resolver este problema, se solicita que, primero, se realice un diseño que sea solución al problema sin explotar el paralelismo (procedural, que puede ser implementado en Python). Luego, a partir de este, realizar una nueva implementación que realice el proceso mediante el uso de la librería OpenMP, explotando el paralelismo del problema. Para ello, se requiere reconocer qué tipo de paralelismo exhibe el problema en cuestión y luego, diseñar la solución del mismo determinando cuáles son los datos/operaciones paralelizables. Se tendrá en cuenta, el nivel de paralelismo alcanzado.

La entrega se hace a travéz del repositorio de GitHub y se deberá demostrar la realizacion del mismo mediante un correcto uso. El repositorio deberá proveer los archivos fuente y cualquier otro archivo asociados a la compilación, archivos  de  proyecto  ”Makefile”  y  el  código correctamente documentado. No debe contener ningún archivo asociado a proyectos de un IDE y se debe asumir que el proyecto podrá ser compilado y corrido por una `tty` en una distribución de Linux con las herramientas típicas de desarrollo. También se deberá entregar un informe (que pude ser en MD en el repo) explicando paso a paso el desarrllo, inluyendo graficos del diseño solución propuesto, justificando en todo momento lo implementrado.
También se deberá investigar acerca de qué utilidades de profiling gratuitas existen y que brinda cada una (un capítulo del informe), optando por una para realizar las mediciones de tiempo de ejecución de la aplicación diseñada.

El informe debe contener gráficos y análisis de comparación entre la ejecución procedural y la distribuida. El informe además debe contener el diseño de la solución y la comparativa de profilers.

## Evaluación
El presente trabajo práctico es individual y deberá entregarse antes de las 23:50ART del día 13 de Mayo de 2021 dejando asentado en el LEV con el archivo de ifnorme. Será corregido y luego se coordinará una fecha para la defensa oral del mismo.

## Documentación
- [NOAA GOES on AWS](https://docs.opendata.aws/noaa-goes16/cics-readme.html)
- [GOES16 Fire Temperature](https://github.com/blaylockbk/pyBKB_v3/blob/master/BB_GOES/mapping_GOES16_FireTemperature.ipynb)
- [Network Common Data Form (NetCDF)](https://www.unidata.ucar.edu/software/netcdf/)
